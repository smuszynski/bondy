![Bondy Logo](logo.png)

> “Know what you own, and know why you own it.“

> Peter Lynch, Fund Manager

---
## Universal Bond Platform
Bond issuing today can be a long and costly process when doing it with a bank.
Once you register with Bondy, we offer cheaper, quicker, easier and more reliable bond issuing thanks to the use of blockchain technology.

---
## Mobile App
We have built an iOS app, where you can issue and buy bonds in an easy, quick, and reliable way.

Take a look at our [Bondy Mobile Repository](https://bitbucket.org/arturolszak/six2016).

---
## Backend
The Backend API is implemented using Ruby on Rails and incorporates the blockchain API from http://coloredcoins.org .

Take a look at our [Bondy API Repository](https://bitbucket.org/smuszynski/bondy).