Rails.application.routes.draw do
  resources :companies, except: [:new, :edit]
  resources :credit_ratings, except: [:new, :edit]
  get '/bitcoins/broadcast', to: 'bitcoins#broadcast_transaction'
  root 'root#index'
end
