class BitcoinsController < ApplicationController

  def broadcast_transaction
    network = Coloredcoins::TESTNET
    api_version = 'v3'
    api = Coloredcoins::API.new(network, api_version)
    tx_hex = get_tx_hex
    wif = get_wif
    result = api.broadcast(tx_hex)
    puts result
  end

  private
  def get_tx_hex
    '01000000019044b8d0941d7bb8f99f0fe80a1a2d5a6a1a0e522c70a2a8fa67725156ef87960100000000ffffffff020000000000000000086a0643430205011018a46100000000001976a91498935f49f43f22e38b65d8c7f7df4a5bfa72eb0788ac00000000'
  end

  def get_wif
    'L5Nc16JPeE4Z7YQoUdyxcNgQKXKqjSTVPP1cinVGVt5m7ZuoAwjn'
  end

end
