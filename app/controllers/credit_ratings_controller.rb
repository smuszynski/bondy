class CreditRatingsController < ApplicationController
  before_action :set_credit_rating, only: [:show, :update, :destroy]

  def index
    @credit_ratings = CreditRating.all
    render json: @credit_ratings
  end

  def show
    render json: @credit_rating
  end

  def create
    @credit_rating = CreditRating.new(credit_rating_params)

    if @credit_rating.save
      render json: @credit_rating, status: :created, location: @credit_rating
    else
      render json: @credit_rating.errors, status: :unprocessable_entity
    end
  end

  def update
    @credit_rating = CreditRating.find(params[:id])

    if @credit_rating.update(credit_rating_params)
      head :no_content
    else
      render json: @credit_rating.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @credit_rating.destroy
    head :no_content
  end

  private

    def set_credit_rating
      @credit_rating = CreditRating.find(params[:id])
    end

    def credit_rating_params
      params.require(:credit_rating).permit(:name)
    end
end
