var bitcoin = require('bitcoinjs-lib');
var request = require('request');

function postToApi(api_endpoint, json_data, callback) {
  console.log(api_endpoint+': ', JSON.stringify(json_data));
  request.post({
    url: 'http://testnet.api.coloredcoins.org:80/v3/'+api_endpoint,
    headers: {'Content-Type': 'application/json'},
    form: json_data
  },
  function (error, response, body) {
    if (error) {
      return callback(error);
    }
    if (typeof body === 'string') {
      body = JSON.parse(body)
    }
    console.log('Status: ', response.statusCode);
    console.log('Body: ', JSON.stringify(body));
    return callback(null, body);
  });
};

var signedTxHex= '01000000019044b8d0941d7bb8f99f0fe80a1a2d5a6a1a0e522c70a2a8fa67725156ef8796010000006a47304402203ef2864b745f3d765ca7d8d5e811b716e56a3651ed7463598a3cf7bdff632434022060107e109211cfb4f8502174ce4ad75912efa676f904f51fd51c1fe62221852e0121033c5d3b08d741b96089fd5d00c64368526adf8fb66a2d76644de4a27f4e4dad83ffffffff020000000000000000086a0643430205011018a46100000000001976a91498935f49f43f22e38b65d8c7f7df4a5bfa72eb0788ac00000000';
var transaction = {
  'txHex': signedTxHex
}

postToApi('broadcast', transaction, function(err, body){
  if (err) {
    console.log('error: ', err);
  }
});
