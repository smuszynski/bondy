class CreateCreditRatings < ActiveRecord::Migration
  def change
    create_table :credit_ratings do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
