class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :logo
      t.string :name
      t.string :description
      t.string :industry
      t.decimal :amount, precision: 8, scale: 2
      t.decimal :bond_yield, precision: 5, scale: 2
      t.string :revenue
      t.references :credit_rating, index: true, foreign_key: true
      t.datetime :maturity_date
      t.integer :face_value
      t.date :issue_date
      t.date :next_coupon_date
      t.decimal :coupon_rate, precision: 5, scale: 2
      t.string :coupon_frequency
      t.integer :available_bonds
      t.integer :seeking_value
      t.integer :seeking_units
      t.integer :raised_value
      t.integer :raised_units

      t.timestamps null: false
    end
  end
end
