# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160305201430) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "logo"
    t.string   "name"
    t.string   "description"
    t.string   "industry"
    t.decimal  "amount",           precision: 8, scale: 2
    t.decimal  "bond_yield",       precision: 5, scale: 2
    t.string   "revenue"
    t.integer  "credit_rating_id"
    t.datetime "maturity_date"
    t.integer  "face_value"
    t.date     "issue_date"
    t.date     "next_coupon_date"
    t.decimal  "coupon_rate",      precision: 5, scale: 2
    t.string   "coupon_frequency"
    t.integer  "available_bonds"
    t.integer  "seeking_value"
    t.integer  "seeking_units"
    t.integer  "raised_value"
    t.integer  "raised_units"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "companies", ["credit_rating_id"], name: "index_companies_on_credit_rating_id", using: :btree

  create_table "credit_ratings", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "companies", "credit_ratings"
end
